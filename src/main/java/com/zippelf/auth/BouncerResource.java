package com.zippelf.auth;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/auth")
public class BouncerResource {

    @GET
    @Path("/admin")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed({"cluster-admin"})
    public String requireClusterAdminRole() {
        return "OK";
    }

    @GET
    @Path("/readonly")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed({"cluster-admin", "cluster-readonly"})
    public String requireReadonlyRole() {
        return "OK";
    }
}
