package com.zippelf.auth;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeBouncerResourceIT extends BouncerResourceTest {

    // Execute the same tests but in native mode.
}
